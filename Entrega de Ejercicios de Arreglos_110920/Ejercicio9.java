/* 9. Escribir un programa que lea una matriz de números enteros y que devuelva
la suma de los elementos positivos de la matriz y la suma de los elementos
negativos.*/
import java.util.Scanner;
public class Ejercicio9{
    public static int Resta= 0 ;
    public static int Suma = 0;
    public static void main(String[] args){ 
        int matriz[][] = new int [3][3];
        Scanner entrada = new Scanner(System.in); 

        for (int i=0;i<matriz.length;i++){              //Insertando datos
            for (int j=0 ; j<matriz.length ; j++){
                System.out.print("Posicion ["+(i+1)+"] ["+(j+1)+"] : ");
                matriz [i][j] = entrada.nextInt();
            }
        }

        System.out.println();
        System.out.println("Matriz ingresada");

        for (int i=0 ; i<matriz.length ; i++){          //Imprimiendo matriz
            for (int j=0 ; j<matriz.length ; j++){
                System.out.print(matriz [i][j]+ " \t");
            }
            System.out.println();
        }

        for (int i=0 ; i<matriz.length ; i++){         //Cálculo de Suma positivas y negativas
            for (int j=0 ; j<matriz.length ; j++){
                if (matriz[i][j] < 0){
                    Resta = Resta + matriz[i][j];
                }else{
                    Suma = Suma + matriz[i][j];
                }
            }
        }
        System.out.println();
        System.out.println("La suma de los numeros positivos es: "+Suma); //Salida
        System.out.println("La suma de los numeros negativos es: "+Resta);  //Salida
        System.out.println("La diferencia de positivos y negativos es de: "+(Suma+Resta));
    }
}
