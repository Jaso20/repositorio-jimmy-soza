import java.util.Scanner;

public class EjercicioN2 {

    public static void main(String[] args) {
        
        double Calif[][] = new double[5][4];
        System.out.println("Se realizara una encuesta para medir la calidad del servicio ofrecido en este restaurante");
        System.out.println("");
        System.out.println("Por favor califique nuestros servicios del 1 al 10 ");
        
        for(short i = 0; i < 5; i++) {
            
            System.out.println("\nCliente #" + (i + 1) + ":");
            
            for(short j = 0; j < 4; j++) {
                
                pregunta(Calif,i,j);
            }
        }
        
        System.out.println("");
        
        prom(Calif);
    }
    
    public static void pregunta(double[][] Calif, short i, short j) {
        
        Scanner in = new Scanner(System.in);
        
        switch(j) {
            
            case 0:
                System.out.println("¿Qué tan buena es la atención de parte de nuestros empleados?");
                Calif[i][j] = in.nextDouble();
                System.out.println("");
                break;
            case 1:
                System.out.println("¿Cómo considera la calidad de la comida?");
                Calif[i][j] = in.nextDouble();
                System.out.println("");
                break;
            case 2:
                System.out.println("¿Qué tan justo le parecen los precios?");
                Calif[i][j] = in.nextDouble();
                System.out.println("");
                break;
            case 3:
                System.out.println("¿Qué tan bueno es el ambiente?");
                Calif[i][j] = in.nextDouble();
                System.out.println("");
                break;
        }
    }
    
    public static void prom(double[][] Calif) {
        
        double prom = 0;
        
        for(byte j = 0; j < 4; j++) {
            
            for(byte i = 0; i < 5; i++) {
                
                prom += Calif[i][j];
            }
            
            prom /= 5;
            
            aspectos(j,prom);
            
            prom = 0;
        }
    }
    
    public static void aspectos(byte j, double prom) {
        
        switch(j) {
            
            case 0:
                System.out.println(" promedio a la pregunta Atención por parte de los empleados es: " + prom);
                break;
            case 1:
                System.out.println("Promedio a la pregunta Calidad de la comida es: " + prom );
                break;
            case 2:
                System.out.println("promedio a la pregunta Justicia del precio es: " + prom);
                break;
            case 3:
                System.out.println("promedio a la pregunta de la encuesta Ambiente es: " + prom);
                break;
        }
    }
}



  