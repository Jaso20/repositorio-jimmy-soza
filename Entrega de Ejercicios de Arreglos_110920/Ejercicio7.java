/* 7. Escribir un programa que lea una matriz de 3 filas y 3 columnas de valores
enteros. A continuación, el programa debe pedir el número de una fila. El
programa deberá devolver el máximo de esa fila.*/

import java.util.Scanner;
public class Ejercicio7 {

    public static int memoria = 0; //Variable global

    public static void main(String[] args){
        boolean cambios =false;
        int fila = 0;
        int numeros [][] = new int [3][3];   //Iniciar matriz
        Scanner leer = new Scanner(System.in);

        for (int i=0 ; i<numeros.length ; i++){  //Numero que inserta el usuario
            for (int j=0 ; j<numeros[i].length ; j++){
                System.out.print("Posicion ["+ (i+1) + "] [" + (j+1) + "] =");
                numeros [i][j] = leer.nextInt();
            }
        }

        System.out.println();

        for ( int i=0 ; i<numeros.length ; i++){  //Mostrando matriz ingresada
            for ( int j=0 ; j<numeros[i].length ; j++){
                System.out.print (numeros[i][j] + "\t");
            }
            System.out.println();
        }

        while(true){   //Metodo de burbuja
            cambios = false;
            for (int i=0 ; i<3 ; i++){
               for (int j=1 ; j<3 ; j++){
                   if (numeros[i][j] < numeros[i][j-1]){
                       memoria = numeros[i] [j-1] ;
                       numeros[i][j-1] = numeros[i][j] ;
                       numeros[i][j] = memoria ;
                       cambios = true;
                   }
               }
           }  
        if (cambios==false)
        break;
        }

        System.out.println();

    

        System.out.print("Ingrese la fila a buscar: \t");
        fila = leer.nextInt();
        System.out.print(numeros[fila-1][2]+ " Es el numero mayor de la fila "+fila);
        
    }
}
