/* 8. Escribir un programa que lea una matriz de enteros de 4 filas y 4 columnas y
a continuación intercambie la fila i con la fila j, siendo i y j dos valores
introducidos por teclado

Nota: En el ejercicio se usaron 2 vectores para hacer el intercambio con las fila y columna, para
no perder ningun dato de la matriz original, es necesario que este un poco desordenado una vez hecho
el intercambio, porque si se intenta poner ordenados, se pierde 1 o 2 datos de la matriz. Es importante
que en el lugar que cruzen la fila y columna en la matriz transformada, tenga en comun el mismo dato en ambas
fila y columna
*/
import java.util.Scanner;
public class Ejercicio8 {

    public static int fila = 0;
    public static int columna = 0;
    public static int n = 3;

    public static void main(String[] args) {
        int vectorF [] = new int [4];
        int vectorC [] = new int [4];
        int matriz[][] = new int [4][4];
     
        Scanner entrada = new Scanner(System.in);
        
        for (int i=0 ; i<matriz.length ; i++){   //Insertar matriz
            for (int j=0 ; j<matriz.length ; j++){
                System.out.print("Posicion ["+(i+1)+"] ["+(j+1)+"] = ");
                matriz [i][j] = entrada.nextInt();
            }
        }

        System.out.println();
        System.out.println("Matriz ingresada");
        for (int i=0 ; i<matriz.length ; i++){   //Mostrar matriz
            for (int j=0 ; j<matriz.length ; j++){
                System.out.print(+matriz[i][j]+"\t");
            }
            System.out.println();
        }
        System.out.println("¿Que fila y columna desea cambiar?");
        System.out.print("Fila: ");
        fila = entrada.nextInt();
        System.out.print("Columna: ");
        columna = entrada.nextInt();

        System.out.println();
        System.out.println();

        for (int j=0;j<vectorF.length;j++){   //vector columna
            vectorF [n]= matriz [fila-1][j];
            n = n-1;
        }

        n=3;

        for (int i=0;i<vectorC.length;i++){   //vector fila
            vectorC [n]= matriz [i][columna-1]; 
            n = n-1;
        }        

        for (int i=0;i<matriz.length;i++){
            matriz [i][columna-1] = vectorF[i];  
        }

        for (int j=0;j<matriz.length;j++){
            matriz [fila-1][j] = vectorC[j];  
        }

        System.out.println("Matriz transformada");
        for (int i=0 ; i<matriz.length ; i++){   //Mostrar Cambiada
            for (int j=0 ; j<matriz.length ; j++){
                System.out.print(+matriz[i][j]+"\t");
            }
            System.out.println();
        }  
    } 
}
