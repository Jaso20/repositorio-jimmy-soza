/*10. Escribir un programa que lea una matriz de 4 filas y 3 columnas, la visualice
por pantalla y a continuación encuentre el mayor y el menor elemento de la
matriz y sus posiciones*/
import java.util.Scanner;
public class Ejercicio10{
    public static int mayor = -10000*100;
    public static int menor = 10000*100;
    
    public static void main(String[] args){
        int matriz [][] = new int [4][3];
        int PosiMayor [] = new int [2];       //Instanciacion y Declaración
        int PosiMenor [] = new int [2];

        Scanner entrada = new Scanner(System.in);

        for (int i=0 ; i<matriz.length ; i++){   //Insertando datos de la matriz
            for (int j=0 ; j<matriz[i].length ; j++){
                System.out.print("A ["+(i+1)+"] ["+(j+1)+"] = ");
                matriz [i][j] = entrada.nextInt();
                if (mayor < matriz[i][j]){       // Condicional numero mayor
                    mayor = matriz [i][j];
                    PosiMayor [0] = i+1;
                    PosiMayor [1] = j+1; 
                }
                if (menor > matriz[i][j]){       // Condicional numero menor
                    menor = matriz [i][j];
                    PosiMenor [0] = i+1;
                    PosiMenor [1] = j+1;
                }
            }
        }

        System.out.println();
        System.out.println("Matriz A");

        for (int i=0 ; i<matriz.length ; i++){   //Mostrando matriz
           for (int j=0 ; j<matriz[i].length ; j++){
                System.out.print(matriz[i][j]+"\t");
            }
            System.out.println("");
        }
        System.out.println();
        System.out.println("El mayor numero es "+mayor+ " y se encuentra en la posicion ["+PosiMayor [0]+"] ["+PosiMayor [1]+"]");  //Salida
        System.out.println("El menor numero es "+menor+ " y se encuentra en la posicion ["+PosiMenor [0]+"] ["+PosiMenor [1]+"]");  //Salida
    }
}
